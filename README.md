# images_in_polygon
```
usage: images_in_poly.py [-h] -j JSON_FILE -p PROPERTIES -s SOURCE [-a]
                         [-d DESTINATION] [-w] [-q] [-v]

Script to find in which polygon a geolocalized image belongs to

optional arguments:
  -h, --help            show this help message and exit
  -j JSON_FILE, --json_file JSON_FILE
                        Path to the geojson file
  -p PROPERTIES, --properties PROPERTIES
                        Geojson properties key used to name the subfolders
  -s SOURCE, --source SOURCE
                        Path to the images folder. Sub folder are scanned too
  -a, --copy_orphan     Copy images located outside of any polygon to the
                        destination/no_area directory
  -d DESTINATION, --destination DESTINATION
                        Path to the destination folder in which the images
                        will be copied
  -w, --write_tag       write area name inside image exif metadata
                        (GpsAreaInformation)
  -q, --quiet           Don't display images results
  -v, --version         show program's version number and exit
```
                        
## installation:

+ Create a virtual environnement: `python3 -m venv image_in_polygon`
+ Move to this environnement: `cd image_in_polygon`
+ Activate this environnement: `source bin/activate`
+ Clone this repository: `git clone https://gitlab.com/cartocite/images_in_polygon.git`
+ Install the requirements: `pip install -r images_in_polygon/requirements.txt`
